import { Component, OnInit } from '@angular/core';
import{ IPersona } from 'src/app/interfaces/persona.interface'

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  persona: IPersona = {
    correo: ''
  };

  vClick: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  verificar(): void{
    console.log('Verificado');
    console.log(this.persona.correo);
  }

}
