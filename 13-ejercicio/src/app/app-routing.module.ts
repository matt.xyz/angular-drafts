import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailComponent } from './components/email/email.component';


const routes: Routes = [
   { path: 'correo', component: EmailComponent},
   { path: '**', pathMatch:'full', redirectTo: 'correo'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
