import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTemComponent } from './form-tem.component';

describe('FormTemComponent', () => {
  let component: FormTemComponent;
  let fixture: ComponentFixture<FormTemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
