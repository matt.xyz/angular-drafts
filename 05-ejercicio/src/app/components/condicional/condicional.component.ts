import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  persona = {
    nombre: 'Akita Fang',
    dir: 'Av. Stephan #098'
  };

  mostrar: boolean = true;
  constructor() { }


  ngOnInit(): void {
  }

}
