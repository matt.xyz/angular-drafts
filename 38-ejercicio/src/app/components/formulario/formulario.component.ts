import { Component, OnInit } from '@angular/core';
import { IPersona } from 'src/app/interfaces/persona.interface'

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  persona: IPersona = {
    nombre: '',
    direccion: '',
    acepto: ''
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar(): void{
    console.log('envio del form');
    console.log(this.persona.nombre);
    console.log(this.persona.direccion);
    
  }

}
