import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  anioActual=new Date;
  anio = this.anioActual.getFullYear();
  mes = this.anioActual.getMonth();
  day = this.anioActual.getDay();

  constructor() {
    
  }

  ngOnInit(): void {
  }

}
