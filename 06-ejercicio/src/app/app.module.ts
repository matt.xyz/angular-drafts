import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { KuromiComponent } from './components/kuromi/kuromi.component';
import { InfoComponent } from './components/info/info.component';
import { APP_ROUTING } from './app.routes';
import { InicioComponent } from './components/inicio/inicio.component';
import { KuromisComponent } from './components/kuromis/kuromis.component';
import { MasComponent } from './components/mas/mas.component';
import { AyudaComponent } from './components/ayuda/ayuda.component';
import { ContacComponent } from './components/contac/contac.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    KuromiComponent,
    InfoComponent,
    InicioComponent,
    KuromisComponent,
    MasComponent,
    AyudaComponent,
    ContacComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
