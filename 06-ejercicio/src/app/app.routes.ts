import { RouterModule, Routes } from "@angular/router";
import { AyudaComponent } from "./components/ayuda/ayuda.component";
import { ContacComponent } from "./components/contac/contac.component";
import { HomeComponent } from "./components/home/home.component";
import { InfoComponent } from "./components/info/info.component";
import { InicioComponent } from "./components/inicio/inicio.component";
import { KuromiComponent } from "./components/kuromi/kuromi.component";
import { KuromisComponent } from "./components/kuromis/kuromis.component";
import { MasComponent } from "./components/mas/mas.component";


const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'kuromi', component: KuromiComponent },
  { path: 'info', component: InfoComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'kuromis', component: KuromisComponent },
  { path: 'mas', component: MasComponent },
  { path: 'ayuda', component: AyudaComponent },
  { path: 'contac', component: ContacComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);